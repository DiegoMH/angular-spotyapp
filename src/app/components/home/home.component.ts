import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SpotifyService } from "../../services/spotify.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  ultimasCanciones: any = [];
  load: boolean;
  error: boolean;
  mensajeError: string;

  constructor(private spotifyService: SpotifyService) {
    this.load = true;
    this.error = false;
    this.spotifyService.getUltimasCanciones()
      .subscribe((data: any) => {
        this.ultimasCanciones = data;
        this.load = false;
      }, (error) => {
        this.mensajeError = error.error.error.message;
        this.load = false;
        this.error = true;
      });
  }

}
