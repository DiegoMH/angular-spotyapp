import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from "../../services/spotify.service";

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html'
})
export class ArtistaComponent implements OnInit {

  artista: any = {};
  topTracks: any[] = [];
  load: boolean;

  constructor(private router: ActivatedRoute, private spotifyService: SpotifyService) {
    this.load = true;
    this.router.params.subscribe(params => {
      this.getArtista(params['id']);
      this.getTopTracks(params['id']);
    });
  }

  ngOnInit(): void {
  }

  getArtista(id: string) {
    this.spotifyService.getArtista(id)
      .subscribe(resp => {
        this.artista = resp;
        this.load = false;
      })
  }

  getTopTracks(id: string) {
    this.spotifyService.getTopTracks(id)
      .subscribe(resp => {
        this.topTracks = resp['tracks'];
        console.log(this.topTracks);
      });
  }

}
