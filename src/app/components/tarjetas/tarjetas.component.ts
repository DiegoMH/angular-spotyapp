import { Route } from '@angular/compiler/src/core';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html'
})
export class TarjetasComponent implements OnInit {

  @Input() items: any[] = [];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  verArtista(item: any) {
    let idArtista: string;
    if (item.type === 'album') {
      idArtista = item.artists[0].id;
    } else {
      idArtista = item.id;
    }
    this.router.navigate(['artistass',idArtista]);
  }

}
