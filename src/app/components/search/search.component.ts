import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {

  artistas: any[] = [];
  load: boolean;

  constructor(private spotify: SpotifyService) { }

  ngOnInit(): void {
  }

  buscarArtista(nombre: string) {
    nombre == '' ? this.load = false : this.load = true;
    this.spotify.getArtistas(nombre)
      .subscribe((data: any) => {
        this.artistas = data;
        this.load = false;
      });
  }

}
