import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { }

  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${query}`;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQCT0HEksWKgT3Jbr2tPhsjpxzdqZo8ff3eo8fuWlSwuhwBog4-LjVPw5XDzsOWbi84qzpicgha3hdDM0xc'
    });
    return this.http.get(url, { headers });
  }

  getUltimasCanciones() {
    return this.getQuery("browse/new-releases?limit=15")
      .pipe(map((data: any) => data.albums.items));
  }

  getArtistas(nomArtista: string) {
    return this.getQuery(`search?q=${nomArtista}&type=artist&market=ES&limit=15&offset=5`)
      .pipe(map(data => {
        return data['artists'].items;
      }));
  }

  getArtista(id: string) {
    return this.getQuery(`artists/${id}`);
  }

  getTopTracks(id: string) {
    return this.getQuery(`artists/${id}/top-tracks?market=es`);
  }

}
